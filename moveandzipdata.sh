#!/bin/bash
# preface

# bckeeper install dir
bckeeperdir="/home/bk/bckeeper/"
# logfile
logfile="/var/log/bckeeper.log"
# mount of destination volume
dstmount="/media/Batstorage"
# mount of batcorder
bcmount="/media/Batcorder"
# save place to temorary store data
bctemp="/media/Batstorage/orig"
# mount batcorder
automount=1
# destination threshold in percent (size to be left free on drive)
dstthreshold=2

errors=0 # no of error

switchsdtodut () {
    if [ -z ${SDMUXPATH} ]; then
        echo "empty SDMUXPATH!" >> $logfile
        return
    fi
    # switch back to dut
    # first check state of SD-Mux
    usbmuxstatus=$(usbsdmux ${SDMUXPATH} get)
    # if not host, something is strange
    if [ "$usbmuxstatus" = "host" ]; then
        # switch to dut
        usbsdmux ${SDMUXPATH} dut
        echo "Usbsdmux: Switched to dut mode."  >> $logfile
    else
        echo "Usbsdmux: Warning, already in dut mode. That is strange."  >> $logfile
    fi
}

switchtodutandexit () {  
    switchsdtodut
    exit $errors; 
}

unmountbc () {
    # unmount bcmount to clear dirtybit
    umount $bcmount
    ret=$?
    now=$(date '+%Y-%m-%d %H:%M:%S')
    if [ $ret != 0 ]; then
        echo "[$now] unmount error ${ret}" >> $logfile
    else
        echo "[$now] ${bcmount} unmounted." >> $logfile
    fi
}

while getopts ":d" o; do
    case "${o}" in
        d)
            debug=1
            ;;
        *)
            usage
            ;;
    esac
done

if [ -z ${1} ]; then
    echo "SDMUXPATH missing!!!" >> $logfile
    exit 1
else
    SDMUXPATH="${1}"
fi

now=$(date '+%Y-%m-%d %H:%M:%S')
echo "--- $now ---"  >> $logfile

#echo "this is a planed error. don't worry." >> $logfile
#exit 1

# check existance of dst
dst=$(mount | grep "$dstmount")
if [ -z "${dst}" ]; then
    echo "Warning: No external storage found. Using internal memory at: $dstmount" >> $logfile
fi
# main
# check state of SD-Mux
usbmuxstatus=$(usbsdmux ${SDMUXPATH} get)
sleep 1;
# if not dut, something is wrong
echo "[$LINENO] Usbsdmux is in: $usbmuxstatus-mode" >> $logfile
if [ "$usbmuxstatus" = "off" ] || [ "$usbmuxstatus" = "dut" ]; then
    # switch to host
    
    usbsdmux ${SDMUXPATH} host
    ret=$?
    if [ $ret != 0 ]; then
        echo "Usbsdmux: Error switching to host mode." >> $logfile
    else
        echo "Usbsdmux: Switched to host mode." >> $logfile
    fi
else
    echo "Usbsdmux: Already in host mode. No changes." >> $logfile
fi

# automount
## find right device
for node in {'a','b','c','d'}
do
    vendorid=$(udevadm info /dev/sd${node} | grep ID_VENDOR_ID | awk -F "=" '/ID_VENDOR_ID/ {print $NF}')
    modelid=$(udevadm info /dev/sd${node} | grep ID_MODEL_ID | awk -F "=" '/ID_MODEL_ID/ {print $NF}')
    if [ ${modelid} == "4041" ] && [ ${vendorid} == "0424" ]; then
        bcnode="/dev/sd${node}"
        break
    fi
done

bcdev="${bcnode}1" # hardcoded partition: 1
echo "bcdev: ${bcdev}" >> $logfile

echo "looking for ${bcdev}..." >> $logfile
for i in {1..10}
do
    sleep 1;
    if [ -b "${bcdev}" ]; then
        result="device exist." >> $logfile
        break
    fi
    printf "." >> $logfile
done
if [ -z "$result" ]; then
    printf "\n"
    echo "Error provisioning device." >> $logfile
    exit 1
fi
printf "\n" >> $logfile

isbcmounted=$(mount | grep ${bcdev} | wc -l)
if [ $automount != 0 ]; then
    if [ "${isbcmounted}" == "0" ]; then
        if ! [ -d $bcmount ]; then
            mkdir $bcmount
            chmod a+rw $bcmount
        fi
        mount -a
        ret=$?
        if [ $ret != 0 ]; then
            echo "mount error: ${ret}" >> $logfile
            errors=$((errors+1));
            switchtodutandexit
        else
            isbcmounted=$(mount | grep ${bcdev} | wc -l)
            if [ "${isbcmounted}" == "1" ]; then
                echo "[$LINENO] BC mounted." >> $logfile
            else
                echo "[$LINENO] Error mounting Batcorder. (${isbcmounted})" >> $logfile
                errors=$((errors+1));
                switchtodutandexit
            fi
        fi
    else
        echo "BC already mounted."
    fi
else
    if [ "${isbcmounted}" == "0" ]; then
        echo "BC not mounted. Please mount BC!"
        errors=$((errors+1));
        switchtodutandexit
    else
        echo "BC already mounted."
    fi
fi

copyerror=1
bctemp2="${bctemp}/$(date -d "yesterday 9:00" '+%Y-%m-%d')" # subfolder for batcorder data
if [ "${isbcmounted}" == "1" ]; then
    # wait until device is mounted
    echo "looking for bcmount: $bcmount" >> $logfile
    for i in {1..10}
    do
        if [ -d "${bcmount}" ]; then
            result="BC mounted." >> $logfile
            break
        fi
        printf "." >> $logfile
        sleep 1;
    done
    if [ -z "$result" ]; then
        printf "\n"
        echo "Error mounting Batcorder." >> $logfile
        errors=$((errors+1));
        switchtodutandexit
    fi
    printf "\n" >> $logfile

    # new data
    allfiles=$(find ${bcmount} -maxdepth 2 -type f | wc -l)
    newrecords=$((10#${allfiles}-10#1 ))
    newrecordsstr="Anzahl neuer Aufnahmen: $newrecords"
    echo $newrecordsstr
    echo $newrecordsstr >> $logfile

    # state of new batcorder data
    echo "Status der Festplatte: ${bcmount}"
    bcstatus=$(df -h --output="size,used,avail,pcent" ${bcmount})
    echo "${bcstatus}"

    # determine size of new data
    #bcused={df .| grep 'bcmount' | awk '{print $3}'}
    #bcused=$(df /home/he/Documents/bckeeper/ | grep 'debianvg-' | awk '{print $3}')
    bcused=$(df --output='used' $bcmount | awk 'END{print $1}')
    echo "data on dut: $bcused" >> $logfile
    dstfree=$(df --output='avail' $dstmount | awk 'END{print $1}')
    echo "free space on dst media: $dstfree" >> $logfile

    dstsizeused=$(df --output='used' $dstmount | awk 'END{print $1}')
    echo "used space on dst media: $dstsizeused" >> $logfile

    dstsizetotal=$((10#$dstsizeused+10#$dstfree))
    margin=$(($dstsizetotal*10#$dstthreshold/100))
    dstfits=$((10#$dstfree-10#$bcused))
    echo "$dstsizetotal, $margin, $dstfits, $dstthreshold" >> $logfile
    if [ $dstfits -lt $margin ]; then
        echo "not enough space on destination" >> $logfile
        errors=$((errors+1));
        unmountbc
        switchtodutandexit
    fi
    # move files to a save place
    now=$(date '+%Y-%m-%d %H:%M:%S')
    echo "[$now] rsync start." >> $logfile
    if [ ! -d "${bctemp2}" ]; then
        mkdir "${bctemp2}"
    fi
    #rsync -a "${bcmount}/" "${bctemp2}"
    cp -a "${bcmount}/LOGFILE.TXT" "${bctemp2}/LOGFILE.TXT"
    cp -a "${bcmount}/"* "${bctemp2}"
    ret=$?
    now=$(date '+%Y-%m-%d %H:%M:%S')
    if [ $ret != 0 ]; then
        echo "[$now] copy error: ${ret}. Emergency copy of LOGFILE.TXT at: /tmp/LOGFILE.TXT" >> $logfile
        cp ${bcmount}/LOGFILE.TXT /tmp/LOGFILE.TXT
        errors=$((errors+1));
    else
        echo "[$now] copy completed. Dest: ${bctemp2}" >> $logfile
        copyerror=0
    fi
fi

# release batcorder - part1
unmountbc

# clean old files
bctempold="${bctemp}/$(date -d '-4 day' '+%Y-%m-%d')"
if [ -d "${bctempold}" ]; then
    rm -r ${bctempold}
    ret=$?
    if [ $ret != 0 ]; then
        echo "cleanup - error: ${ret} while removing: ${bctempold}" >> $logfile
    else
        echo "cleanup - removed: ${bctempold}" >> $logfile
    fi
else
    echo "cleanup - nothing to do." >> $logfile
fi

## remove mount dir (mount -a needs a folder)
#if [ $automount == 1 ]; then
#    echo "removing mount dir: $bcmount" >> $logfile
#    if [ -d ${bcmount} ]; then
#        #rmdir ${bcmount}
#        ret=$?
#        if [ $ret != 0 ]; then
#            echo "Could not remove: ${bcmount}, error: $ret" >> $logfile
#        fi
#    fi
#fi

# restore clean disk
if [ $copyerror -eq 0 ]; then
    echo "restoring clean SDcard to ${bcnode}..." >> $logfile
    #dd if=${bckeeperdir}fattable202111301735 of=$bcnode bs=4KB count=128000 >> $logfile
    #dd if=${bckeeperdir}fattable_timeron_202112021430 of=$bcnode bs=4KB count=128000 >> $logfile
    dd if=${bckeeperdir}fattable_timeron_202202231700 of=$bcnode bs=4KB count=128000 >> $logfile
    ret=$?
    now=$(date '+%Y-%m-%d %H:%M:%S')
    if [ $ret != 0 ]; then
        echo "[$now] dd error: ${ret}" >> $logfile
    else
        echo "[$now] dd completed." >> $logfile
    fi
else
    echo "WARNING! Leaving SDcard untouched. Data will remain." >> $logfile
fi

#    # clean up files on BC
#    # removing files or format SD are the options. Remove files not testet.
#    for d in $bcmount/*/ ; do
#        echo "removing: $d"  >> $logfile
#        rm -r $d
#    done
#    echo "BC clean."  >> $logfile
#
#    # Create fresh logfile and adjust timestamp
#    cp LOGFILE.TXT $bcmount/
#    touch -m -t "202111072325.00" $bcmount/LOGFILE.TXT
#    echo "fresh logfile installed." >> $logfile


# TODO: check filesystem? 

# release batcorder - part2 
switchsdtodut

# TODO: send progress to server
if [ $copyerror -eq 0 ]; then
    # timestamp
    now=$(date '+%Y-%m-%d %H:%M:%S')
    echo "[$now] Starting compression.">> $logfile

    # we have our files in bctemp2 hopefully
    # if only logfile exists, there's not much we can do.
    numoffolders=$(find ${bctemp2} -maxdepth 1 -type d -print| wc -l) # including bctemp2 itself
    echo "[$LINENO] Folders on BC: $numoffolders" >> $logfile
    if [ $numoffolders == "1" ]; then
        echo "Warning: Expecting new folder. Is the Batcorder started in timer-mode?" >> $logfile
        logfilesizenow=$(stat --printf="%s" "${bctemp2}/LOGFILE.TXT")
        echo "[$LINENO] LOGFILE.TXT size: $logfilesizenow" 
        if [ $logfilesizenow != "162" ]; then
            filename=$(date +%Y%m%d-%H%M%S)
            zipstatus=$(7zr a "$dstmount/$filename.7z" "$bctemp2/LOGFILE.TXT")
            ret=$?
            if [ $ret != 0 ]; then
                echo "$zipstatus"
                reason=$(printf "$zipstatus" | awk 'END{print}')
                echo "7zr Error: $ret, $reason" >> $logfile
                exit 1
            fi
            echo "Logfile zipped." >> $logfile
        else
            # no changes. Nothing to do.
            echo "ERROR: No chnanges. Logfile . Is Batcorder running? Is filesystem ok?" >> $logfile
        fi
    else
        # move files and compress
        for d in ${bctemp2}/*/ ; do
            echo "[${LINENO}] Folder: ${d}"
            echo $d >> $logfile
            folder=$(echo $d | awk -F'/' '{print $(NF-1)}')
            if ! [ -z "${folder}" ]; then
                yd=$(date -d "yesterday 9:00" '+%Y-%m-%d')
                zipdest="${dstmount}/${yd}"
                if [ ! -d "${zipdest}" ]; then
                    mkdir "${zipdest}"
                fi
                # zip folder and add logfile
                echo "Folder: ${yd}/$folder" >> $logfile
                echo "[$LINENO] Folder: $yd/${folder} d: ${d}"
                zipstatus=$(7zr -v1g a "$dstmount/${yd}/$folder.7z" $d)
                ret=$?
                if [ $ret != 0 ]; then
                    reason=$(printf "$zipstatus" | awk 'END{print}')
                    echo "7zr Error: $ret, $reason" >> $logfile
                    exit 1
                else
                    echo "Folder content ziped." >> $logfile
                fi
                echo "[$LINENO] Packing LOGFILE.TXT"
                zipstatus=$(7zr a "${dstmount}/${yd}/${folder}_LOG.7z" "${bctemp2}/LOGFILE.TXT")
                ret=$?
                if [ $ret != 0 ]; then
                    reason=$(printf "$zipstatus" | awk 'END{print}')
                    echo "7zr Error [$LINENO]: $ret, $reason" >> $logfile
                    echo "7zr Error [$LINENO]: $ret, $reason"
                else
                    echo "Logfile ziped." >> $logfile
                fi
                
            else
                # zip logfile, if no folder
                zipstatus=$(7zr a "${dstmount}/${yd}/${folder}_LOG2.7z" "${bctemp2}/LOGFILE.TXT")
                ret=$?
                if [ $ret != 0 ]; then
                    reason=$(printf "$zipstatus" | awk 'END{print}')
                    echo "7zr Error: $ret, $reason" >> $logfile
                    echo "7zr Error [$LINENO]: $ret, $reason"
                else
                    echo "Logfile zipped." >> $logfile
                fi
                
            fi
        done
        echo "Folders zipped." >> $logfile
    fi

    now=$(date '+%Y-%m-%d %H:%M:%S')
    echo "[$now] Finished compression.">> $logfile
else
    now=$(date '+%Y-%m-%d %H:%M:%S')
    echo "[$now] scipped compression because of copyerror.">> $logfile
fi

# state of dest-storage
echo "Status der Festplatte: ${dstmount}"
dststatus=$(df -h --output="size,used,avail,pcent" ${dstmount})
echo "${dststatus}"

echo "done."  >> $logfile
