#!/bin/bash

# keep wifi alive

host="192.168.0.1"
iface="wlan0"

checkcon () {
  now=$(date "+%Y.%m.%d %H:%M:%S")
  packetslost=$(ping -c4 ${host} | grep -oP '\d+(?=% packet loss)')
  ret=$?
}

checkcon
if [ $ret -ge 1 ]; then
  echo "[${now}] Network check: Troubble with network connection. Restarting ${iface}. Reason: $ret"
  if [ ! -z "${packetslost}" ]; then
    echo "[${now}] Network check: Troubble with network connection. Restarting ${iface}. Packets lost: ${packetslost}%"
  fi

  ifconfig ${iface} down
  sleep 30
  ifconfig ${iface} up
  sleep 20
  
  checkcon
  if [ $ret -ge 1 ]; then
    echo "[${now}] Network check: Troubble with network connection. Reason: $ret"
    if [ ! -z "${packetslost}" ]; then
      echo "[${now}] Network check: restarted ${iface}. Packets lost: ${packetslost}%"
    fi
  else
    echo "[${now}] Network check: restarted ${iface}. Packets lost: ${packetslost}%"
  fi

else
    echo "[${now}] Network check: passed. Packets lost: ${packetslost}%"
fi

