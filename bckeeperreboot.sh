#!/bin/bash

# current time
now=$(date "+%Y.%m.%d %H:%M:%S")

# check if bckeeper finished
bckprocess="bckeeper.sh"
if [ "$(ps -ef | grep "${bckprocess}" | wc -l)" -gt 1 ]; then 
    echo "[${now}] ERROR! Bckeeper running. Aborting restart."; 
    exit 1;
else 
    echo "[${now}] Restarting bckeeper.";
    shutdown -r;
fi
exit 0;
