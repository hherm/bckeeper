#!/usr/bin/bash
SDMUXVENDORID="0424" # "0424"
SDMUXMODELID="4041" # "4041"

sdmuxparent=""
BUSPATH="/sys/bus/usb/devices/"

DEBUG=0
if [ ${DEBUG} -eq 1 ]; then
    echo "Debugging enabled. DEBUG: ${DEBUG}"
fi
for devices in $(ls "${BUSPATH}"); 
do 
    if [ -f "${BUSPATH}${devices}/idVendor" ]; then 
        devid=$(cat "${BUSPATH}${devices}/idVendor")
        devmodel=$(cat "${BUSPATH}${devices}/idProduct")
        if [ ${DEBUG} -eq 1 ]; then
            echo "[$LINENO]1devid: ${devid} modelid: ${devmodel}"
        fi
        if [ "${devid}" == "${SDMUXVENDORID}" ] && [ "${devmodel}" == "${SDMUXMODELID}" ]; then
            if [ ${DEBUG} -eq 1 ]; then
                echo ${devices}
            fi
            sdmuxparent=${devices}
            break
        fi
    fi
done

if [ -z ${sdmuxparent} ]; then
    echo "no parent found."
    exit 1
else
    if [ ${DEBUG} -eq 1 ]; then
        echo "[$LINENO]sdmuxparent: $sdmuxparent"
	fi
fi

targetpath=${sdmuxparent}
if [ ${DEBUG} -eq 1 ]; then
    echo "[$LINENO]targetpath: $targetpath"
fi


for sgdev in $(ls /dev/sg*); do
    udevpath=$(udevadm info "$sgdev" | grep "P: " | grep "${targetpath}" | awk -F "/" '/P: /{print $NF}')
    if [ ${DEBUG} -eq 1 ]; then
        echo "[$LINENO]${sgdev}: $udevpath"
    fi
 	if ! [ -z ${udevpath} ]; then
        echo $udevpath
        break
    fi
done

