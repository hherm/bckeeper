#!/usr/bin/bash
# the core script, triggered by cronjob
# need to run as root.
# author: hherm
# licence: GPLv3
# created: 2021-11-20
version="0.2u00"

bckeeperInstallDir="/usr/local/lib/bckeeper/"
bckeeperLog="/var/log/bckeeper.log"
bckeeperLogZip="/tmp/bckeeperlog.gz"
mailto=""
mailsubject="bckeeper"

. /etc/bckeeper/bckeeper.conf

errors=" "
errorcollector=0

DEBUG=0
while getopts ":d" o; do
    case "${o}" in
        d)
            DEBUG=1
            ;;
        *)
            usage
            ;;
    esac
done

now=$(date '+%Y-%m-%d %H:%M:%S')
echo "--- $now ---"  >> $bckeeperLog

if [ $DEBUG -eq 1 ]; then
    echo "current PATH is: $PATH" >> $bckeeperLog
fi
# get status of sdmux / no sdmux, no data
sdmuxusb=$(lsusb | grep "0424:4041")
if [ -z "${sdmuxusb}" ]; then
    echo "Usbsdmux: USB device not recognised. Check connection."
else
    echo "Usbsdmux: Connected." >> $bckeeperLog
    SDMUX=$(/home/bk/bckeeper/getrightdevpath.sh)
    SDMUXDEV="/dev/${SDMUX}"
    echo "Usbsdmux found at: ${SDMUXDEV}" >> $bckeeperLog
    if [ -z ${SDMUX} ]; then
        echo "Usbsdmux not found!!!!"
    else
        echo "Usbsdmux found at: ${SDMUXDEV}" >> $bckeeperLog
    fi
fi

# run main
if [ $DEBUG -eq 1 ]; then
    echo "${bckeeperInstallDir}moveandzipdata.sh ${SDMUX}"
fi
if [ -f $bckeeperInstallDir"moveandzipdata.sh" ] && ! [ -z ${SDMUX} ]; then
    keeperstatus=$($bckeeperInstallDir"moveandzipdata.sh" ${SDMUXDEV})
    #keeperstatus=$(${bckeeperInstallDir}dummy.sh)
    ret=$?
    if [ $ret != 0 ]; then
        message="E: moveandzipdata. ($ret)"
        echo $message >> $bckeeperLog
        errors+="$message\n"
        errorcollector+=6
    fi
else
    message="E: moveandzipdata missing."
    echo $message >> $bckeeperLog
    echo $message
    errors+="$message\n"
    errorcollector+=7
fi

#echo "[$LINENO] DEBUG STOP."
#exit 0

# mail the status
## compose body
mailbody=""
if ! [ -z "${keeperstatus}" ]; then
    mailbody+="${keeperstatus}\n"
fi

if [ $errorcollector != 0 ]; then
    echo "[$LINENO] got errors."
    gzip -k -f -c $bckeeperLog > $bckeeperLogZip
    errorattachment="-a $bckeeperLogZip"
    errorbody="\nErrors in: $errorcollector\n$errors"
    mailsubject+=": E"
fi

if [ $DEBUG -eq 1 ]; then
    echo -e "[$LINENO] ${mailbody}"
    echo -e "[$LINENO] $mailsubject"
    	
fi

mailbody+="Version: $version\n"
if ! [ -z "${errorbody}" ]; then
    mailbody+="$errorbody\n"
fi
mutterror=$(echo -e "$mailbody" | mutt -s "$mailsubject" "${mailto}" ${errorattachment} 2>&1)
ret=$?
if [ $ret -ne 0 ]; then
    echo "Mutt: $mutterror" | mutt -s "$mailsubject" "${mailto}"
	echo "Mutt: Error ($ret) ${mutterror}" >> $bckeeperLog
    errors+="Mutt: Error $ret\n"
    echo -e "[bk:$LINENO] $errors"
fi

# cleanup
if [ -f "$bckeeperLogZip" ]; then
    rm "$bckeeperLogZip"
    echo "[$LINENO] removed $bckeeperLogZip"
fi

# done.
